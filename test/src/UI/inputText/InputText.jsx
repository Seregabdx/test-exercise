import React from "react";
import style from "./InputText.module.scss";

const InputText = ({ placeholder, value, setValue, errorText }) => {
  return (
    <div className={style.inputContainer}>
      <input
        className={style.input}
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      {errorText ? (
        <div className={style.errorText}>{errorText}</div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default InputText;
