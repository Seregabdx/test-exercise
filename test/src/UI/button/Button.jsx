import React from "react";
import style from "./Button.module.scss";

const Button = ({ children, ...props }) => {
  return (
    <button {...props} className={style.Btn}>
      {children}
    </button>
  );
};

export default Button;
