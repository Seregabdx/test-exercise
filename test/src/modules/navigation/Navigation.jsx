import React, { useEffect } from "react";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import Menu from "../../components/menu/Menu";

const Navigation = () => {
  let location = useLocation();
  let navigate = useNavigate();
  useEffect(() => {
    if (location.pathname == "/") {
      return navigate("/main");
    }
  }, []);

  return (
    <div>
      <Menu />
      <main>
        <Outlet />
      </main>
    </div>
  );
};

export default Navigation;
