import { Routes, Route, useNavigate } from "react-router-dom";
import "./App.css";
import Navigation from "./modules/navigation/Navigation";
import Login from "./page/login/Login";
import Main from "./page/main/Main";
import Profile from "./page/profile/Profile";

import Registration from "./page/registration/Registration";

function App() {
  const navigate = useNavigate();
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Navigation />}>
          <Route path="main" element={<Main />} />
          <Route path="login" element={<Login />} />
          <Route path="registration" element={<Registration />} />
          <Route path="profile" element={<Profile />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
