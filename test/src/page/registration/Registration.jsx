import React from "react";
import { useState } from "react";
import Button from "../../UI/button/Button";
import InputText from "../../UI/inputText/InputText";
import style from "./Registration.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Modal from "../../components/modal/Modal";

const Registration = () => {
  const [userData, setUserData] = useState({
    name: "",
    login: "",
    password: "",
    repeatPassword: "",
  });
  const [error, setError] = useState("");

  const [update, setUpdate] = useState(false);
  const dispatch = useDispatch();
  const users = useSelector((state) => state);
  const navigate = useNavigate();
  const [modal, setModal] = useState(false);

  function submit() {
    setUpdate(true);

    if (
      userData.name &&
      userData.login &&
      userData.password &&
      userData.password === userData.repeatPassword
    ) {
      if (
        !users.find((usr) => {
          let condition = usr.login === userData.login;
          return condition;
        })
      ) {
        dispatch({ type: "USER_REGISTRATION", payload: userData });
        setModal(true);
        setError("Успешно");
      } else {
        setModal(true);
        setError("Такой пользователь уже существует");
      }
    }
  }

  return (
    <div className={style.registrationContainer}>
      <div className={style.registrationContainer__title}>Регистрация</div>
      <div className={style.registrationContainer__name}>
        <InputText
          placeholder={"Введите имя"}
          value={userData.name}
          setValue={(val) => setUserData({ ...userData, name: val })}
          errorText={update && !userData.name ? "Заполните поле" : ""}
        />
      </div>
      <div className={style.registrationContainer__email}>
        <InputText
          placeholder={"Введите логин"}
          value={userData.login}
          setValue={(val) => setUserData({ ...userData, login: val })}
          errorText={update && !userData.login ? "Заполните поле" : ""}
        />
      </div>
      <div className={style.registrationContainer__password}>
        <InputText
          placeholder={"Введите пароль"}
          value={userData.password}
          setValue={(val) => setUserData({ ...userData, password: val })}
          errorText={update && !userData.password ? "Заполните поле" : ""}
        />
      </div>
      <div className={style.registrationContainer__password}>
        <InputText
          placeholder={"Повторно введите пароль"}
          value={userData.repeatPassword}
          setValue={(val) => setUserData({ ...userData, repeatPassword: val })}
          errorText={
            userData.password === userData.repeatPassword
              ? ""
              : userData.repeatPassword.length > 0
              ? "Пароли не совпадают"
              : ""
          }
        />
      </div>
      <div className={style.registrationContainer__button}>
        <Button
          onClick={() => {
            submit();
          }}
        >
          Зарегистрироваться
        </Button>
      </div>
      <Modal vision={modal} setVision={setModal} text={error}>
        {error != "Успешно" ? (
          <>
            <div className={style.modalTitle}>Ошибка</div>
            <div className={style.modalText}>{error}</div>
          </>
        ) : (
          <>
            <div className={style.modalTitle}>
              Пользователь с таким логином зарегистрирован
            </div>
            <div className={style.buttonNavigation}>
              <Button onClick={() => navigate("/login", { replace: true })}>
                На страницу авторизации
              </Button>
            </div>
          </>
        )}
      </Modal>
    </div>
  );
};

export default Registration;
