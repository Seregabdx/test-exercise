import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Button from "../../UI/button/Button";
import style from "./Profile.module.scss";
import profileImg from "../../assets/profile.png";

const Profile = () => {
  let user =
    localStorage.getItem("user") != "undefined"
      ? JSON.parse(localStorage.getItem("user"))
      : {
          name: "",
          login: "",
          password: "",
        };
  const dispatch = useDispatch();
  const navigate = useNavigate();

  return (
    <div className={style.profileContainer}>
      <div className={style.profileContainer__image}>
        <img src={profileImg} alt="" />
      </div>
      <div className={style.profileContainer__infoBlock}>
        <div className={style.name}>{"Имя: " + user.name}</div>
        <div className={style.login}>{"Логин: " + user.login}</div>
        <div className={style.password}>{"Пароль: " + user.password}</div>
        <div className={style.logoutButton}>
          <Button
            onClick={() => {
              dispatch({
                type: "USER_LOGOUT",
                payload: JSON.parse(localStorage.getItem("user")),
              });
              navigate("/main", { replace: true });
            }}
          >
            Выйти
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Profile;
