import React from "react";
import { useState } from "react";
import Button from "../../UI/button/Button";
import InputText from "../../UI/inputText/InputText";
import style from "./Login.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [userData, setUserData] = useState({
    name: "",
    login: "",
    password: "",
  });

  const [update, setUpdate] = useState(false);
  const dispatch = useDispatch();
  const users = useSelector((state) => state.usersData);
  const navigate = useNavigate();

  function submit() {
    setUpdate(true);

    if (userData.name && userData.login && userData.password) {
      dispatch({ type: "USER_LOGIN", payload: userData });
      navigate("/main", { replace: true });
    }
  }

  return (
    <div className={style.registrationContainer}>
      <div className={style.registrationContainer__title}>Вход</div>
      <div className={style.registrationContainer__name}>
        <InputText
          placeholder={"Введите имя"}
          value={userData.name}
          setValue={(val) => setUserData({ ...userData, name: val })}
          errorText={update && !userData.name ? "Заполните поле" : ""}
        />
      </div>

      <div className={style.registrationContainer__email}>
        <InputText
          placeholder={"Введите логин"}
          value={userData.login}
          setValue={(val) => setUserData({ ...userData, login: val })}
          errorText={update && !userData.login ? "Заполните поле" : ""}
        />
      </div>

      <div className={style.registrationContainer__password}>
        <InputText
          placeholder={"Введите пароль"}
          value={userData.password}
          setValue={(val) => setUserData({ ...userData, password: val })}
          errorText={update && !userData.password ? "Заполните поле" : ""}
        />
      </div>

      <div className={style.registrationContainer__button}>
        <Button
          onClick={() => {
            submit();
          }}
        >
          Войти
        </Button>
      </div>
    </div>
  );
};

export default Login;
