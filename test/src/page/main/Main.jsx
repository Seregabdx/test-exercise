import React from "react";
import style from "./Main.module.scss";

const Main = () => {
  return (
    <div className={style.mainPage}>
      <div className={style.mainPage__welcomeMessage}>
        Добро пожаловать на главную страницу,
      </div>
      <div className={style.mainPage__name}>
        {localStorage.getItem("user") != "undefined"
          ? JSON.parse(localStorage.getItem("user")).name
          : "Гость"}
      </div>
    </div>
  );
};

export default Main;
