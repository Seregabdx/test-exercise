import { createStore } from "redux";
import { usersData } from "./reducers/usersData";

export const store = createStore(usersData);
