import docJSON from "../../users.json";

const initialState = docJSON.users;

export const usersData = (state = initialState, action) => {
  switch (action.type) {
    case "USER_REGISTRATION":
      return [
        ...state,
        {
          name: action.payload.name,
          login: action.payload.login,
          password: action.payload.password,
        },
      ];

    case "USER_LOGIN":
      let index = undefined;
      let valid = state.find((usr, ind) => {
        let condition =
          usr.name === action.payload.name &&
          usr.login === action.payload.login &&
          usr.password === action.payload.password;
        if (condition) index = ind;
        return condition;
      });

      if (valid) {
        localStorage.setItem(
          "user",
          JSON.stringify({
            name: action.payload.name,
            login: action.payload.login,
            password: action.payload.password,
            isLogin: true,
          })
        );
        return state;
      } else {
        localStorage.setItem("user", undefined);
        return state;
      }

    case "USER_LOGOUT":
      localStorage.setItem("user", undefined);
      return state;

    default:
      return state;
  }
};
