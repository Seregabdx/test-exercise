import React, { useEffect } from "react";
import style from "./Modal.module.scss";

const Modal = ({ vision, setVision, text, children }) => {
  const rootClasses = [style.Mmodal];

  useEffect(() => {
    setVision(vision);
  }, []);

  if (vision) {
    rootClasses.push(style.active);
  }

  return (
    <div className={rootClasses.join(" ")} onClick={() => setVision(!vision)}>
      <div className={style.MmodalContent} onClick={(e) => e.stopPropagation()}>
        <div className={style.closeButton} onClick={() => setVision(false)}>
          <span> ╳</span>
        </div>
        {children}
      </div>
    </div>
  );
};

export default Modal;
