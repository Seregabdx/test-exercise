import React from "react";
import logo from "../../assets/logo.png";
import profile from "../../assets/profile.png";
import { NavLink, useNavigate } from "react-router-dom";
import style from "./Menu.module.scss";

const Menu = () => {
  let navigate = useNavigate();
  return (
    <header>
      <div className={style.logoContainer}>
        <img
          className={style.logoContainer__logo}
          src={logo}
          alt="Лого"
          onClick={() => navigate("/main")}
        />
      </div>
      <div className={style.linkContainer}>
        <NavLink to="main" className={style.linkContainer__link}>
          На главную
        </NavLink>
        <NavLink
          to={
            localStorage.getItem("user") != "undefined"
              ? "profile"
              : "registration"
          }
          className={style.linkContainer__link}
        >
          Регистрация
        </NavLink>
        <NavLink
          to={localStorage.getItem("user") != "undefined" ? "profile" : "login"}
          className={style.linkContainer__link}
        >
          Профиль
        </NavLink>
      </div>
      <div
        className={style.profileContainer}
        onClick={() => {
          navigate(
            localStorage.getItem("user") != "undefined" ? "profile" : "login"
          );
        }}
      >
        <img className={style.profileContainer__icon} src={profile} alt="" />
        <div className={style.profileContainer__name}>
          {localStorage.getItem("user") != "undefined"
            ? JSON.parse(localStorage.getItem("user")).name
            : "Гость"}
        </div>
      </div>
    </header>
  );
};

export default Menu;
